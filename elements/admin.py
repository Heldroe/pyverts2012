from elements.models import Element, Photo
from django.contrib import admin

admin.site.register(Element)
admin.site.register(Photo)